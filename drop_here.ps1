#drop_here.ps1
param ( [Parameter(ValueFromRemainingArguments=$true)] $DroppedFilesWrapper )


<#
This script will process an intermixed list of directory and file names passed to a 
PowerShell script when dropped in Windows Explorer on a properly configured accompanying
a .bat file.  The PowerShell script or .bat file can also be called directly
from the command line.

-- This function will accept multiple intermixed directory and file names.  
-- It will check that the files exist, and expand any directories recursively to the depth
   specified (it defaults to 0, the top level).
-- The 'Include' parameter allows limiting what is included, for example: -Include *.doc,*.docx
   It defaults to a $Null string array which will include everything found.  
-- Relative file names and wildcard characters are accepted. 
-- The function returns fully qualified file objects.
-- Update 10/2021:  This works well with PowerShell 7.1.  I found a bug in PowerShell 5.1
   that affects operation when dropping multi-level directories if you supply an "-Include"
   parameter at the same time; the "-Depth 0" parameter is not honored in the "Get-ChildItem"
   command and you get results for any lower sub-directories as well. This is not
   an issue if you are not using "-Include", or if you only drop files or single level
   directories. See additional notes about possible solutions or workarounds (if this is an
   issue for you) toward the end of my web posting about this script on "stackoverflow".
-- Note: "-Include $Null" (which is set as default) acts as if no parameter is present
   and in that case the "-Depth" parameter is honored in PowerShell 5.1 (this is not the
   case for "-Include '' ").
#>
#inspired from:
#https://stackoverflow.com/questions/2819908/drag-and-drop-to-a-powershell-script/67813984#67813984

[string]$Depth       = "0"
[string[]]$Include   = $Null

if ( $DroppedFilesWrapper.Count -eq 0 ) {
   # Makes sure there is at least one parameter, or Get-ChildItem defaults to all in current directory.
   Write-Host "Supply directory and/or file names. Wildcards are allowed." -ForegroundColor Red
   Exit 
}

[bool]$err = $false
foreach ($f in $DroppedFilesWrapper) {
    if ( ( [string]::IsNullOrWhiteSpace($f) ) -or ( (Test-Path -Path $f) -eq $false ) ) {
    Write-Host "ERROR--File or directory does not exist: " -NoNewline  -ForegroundColor Red
    Write-Host $f
    $err = $True
    }
}
if ( $err ) { Exit }

$Droppedfiles = Get-ChildItem -Path $DroppedFilesWrapper -File -Recurse -Depth $Depth -Include $Include -ErrorAction Stop
# '-Depth 0' limits expansion of files in directories to the top level.
# '-Include $Null' will include everything.

Write-Host -f Cyan "`n`nStored to Variable [DroppedFiles]`n`n"
Write-Output $DroppedFiles


